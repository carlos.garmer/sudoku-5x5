"""
A01633757 Carlos García Mercado
"""

import random
import os

def inicio():
    """
    Esta función imprime el logo del juego
    """
    print("""
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@  (@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@     @@@@@@@@@@@@@@@@@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@           @@@@@@@@@@@@@@@@@@@@@@@@@@@@     @@@@@@@@@@@@@@@@@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@    @@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@     @@@@@@@@@@@@@@@@@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@      @@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@     @@@@@@@*   @@@@@@@@    @@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@         @@@@    @@@@@    @@@              @@@            @@@@    @@     @@@@    @@@@@    &@@
@@@@@@@@@@@,      @@    @@@@@    @@     @@@@@     @@    &@@@@@    @@@         @@@@@@    @@@@@    &@@
@@@@   @@@@@@     @@    @@@@@    @@     @@@@@     @@    @@@@@@    @@@         @@@@@@    @@@@@    @@@
@@@              @@@             @@@              @@@            @@@@    @@      @@@(            @@@
@@@@@@@.     @@@@@@@@@@*     .@@@@@@@@@    @@_____@@@@@@.     @@@@@@@____@@@@@____%@@@@@      @@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    """)
def instru():
    """
    Esta función imprime las instrucciones
    """
    print("""
    
    Tu objetivo es rellenar el sudoku, donde las celdas no tienen un número asignado

    Reglas:
    Solo usa valores entre 1 y 5, no incluyas el cero
    No puedes repetir números en cada sección de color
    No puedes repetir números en cada fila 
    No puedes repetir números en cada columna

    Escribe LISTO para revisar tu respuesta
    Escribe RESPUESTA para rendirte

    """)

def nivel():
    """
    Esta función permite selecionar el nivel y determina cuntas respuesta promedio dará antes de finalizar
    """
    nivel = input("""
    
    Seleccione su nivel

    Escriba 1 para nivel FÁCIL, que mostrará 10 celdas ya contestadas
    Escriba algo diferente a 1 para nivel DIFÍCIL, que mostrará 5 celdas ya contestadas

    >>>""")
    if nivel == "1":
        n = 15
    else:
        n= 20
    limpia()
    return nivel,n

def limpia():
    """
    Esta función limpia la terminal
    """
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def sudoku():
    """
    Esta función regresa un sudoku random de una lista
    """
    s = [ [["05","01","03","04","02"],["04","02","05","03","01"],["02","03","04","01","05"],["01","04","02","05","03"],["03","05","01","02","04"]],
           [["04","05","01","02","03"],["03","02","05","04","01"],["02","01","04","03","05"],["05","03","02","01","04"],["01","04","03","05","02"]],
           [["01","03","02","04","05"],["05","04","03","02","01"],["04","02","05","01","03"],["03","01","04","05","02"],["02","05","01","03","04"]],
           [["03","04","01","05","02"],["02","05","03","01","04"],["05","01","04","02","03"],["04","02","05","03","01"],["01","03","02","04","05"]],
           [["01","04","02","05","03"],["05","03","04","02","01"],["03","02","05","01","04"],["02","01","03","04","05"],["04","05","01","03","02"]] ]
    return random.choice(s)

def esconder(niv,ma):
    """
    Esta función recibe el nivel selcionado y el sudoku selecionado de manera aleatoria, se regresa
    una matriz con algunos valores de la matriz respuesta esto depende del nivel se muestra:
    5 celdas si es dificl
    o 10 si es facil
    """
    escondidas = [["A1","A2","A3","A4","A5"],["B1","B2","B3","B4","B5"],["C1","C2","C3","C4","C5"],["D1","D2","D3","D4","D5"],["E1","E2","E3","E4","E5"]]
    for i in range(5):
        celda = random.randint(0,4)
        if niv == "1":
            celda2 = random.randint(0,4)
            while celda == celda2 :
                celda2 = random.randint(0,4)
            escondidas[i][celda2] = ma[i][celda2]
        escondidas[i][celda] = ma[i][celda]
    return escondidas

def responder(ma):
    """
    Esta función permite rellenar las celdas
    Pide la fila con letras mayusculas, el número de la columna y el valor a sustituir,
    regresa la matriz escondida con su nuevo valor
    """
    fil = input("Ingrese la letra de la fila a modificar >>>")
    while fil != "A" and fil != "B" and fil != "C" and fil != "D" and fil != "E":
        print("Error, asegúrese que la letra que esta ingresando es A,B,C,D o E")
        fil = input("Ingrese la letra de la fila a modificar >>>")
    fila = ord(fil.upper()) -65
    col = int(input("Ingrese el número de la columna a modificar >>>"))
    while col> 5 or col<1:
        print("Error, asegúrese que el número que esta ingresando es mayor que 0 y menor que o igual a 5")
        col = int(input("Ingrese el número de la columna a modificar >>>"))
    colu =  col -1
    valor = int(input("Ingrese el número a poner en la celda >>>"))
    while valor> 5 or valor<1:
        print("Error, asegúrese que el valor que esta ingresando es mayor que 0 y menor que o igual a 5")
        valor = int(input("Ingrese el número a poner en la celda >>>"))
    ma[fila][colu] = "0" + str(valor)
    imprimir(ma)
    limpia()
    return ma

def verificar(res,esc):
    """
    Esta función verifica la respuesta por el usuario y la compara con la que se selección en la función sudoku
    """
    if res == esc:
        print("G A N A S T E!!!!!")
    else:
        print("ALGO HICISTE MAL :(")
        print("esta debia de ser tu respuesta:")
        imprimir(sudoku)

def colorear(m,x,y,c):
    """
    Esta función permite imprimir un valor de cierto color
    """
    print(f"\033[0;{c}m"+ str(m[x][y]) +'\033[0;m', end="  |  ")

def imprimir(m):
    """
    Esta función imprime la plantilla del tablero
    """
    print("____________________________________")
    print("|  ", end = "")
    colorear(m,0,0,34) #1
    colorear(m,0,1,34)
    colorear(m,0,2,34)
    colorear(m,0,3,32) #2
    colorear(m,0,4,32)
    print()
    print("____________________________________")
    print("|  ", end = "")
    colorear(m,1,0,34)
    colorear(m,1,1,34)
    colorear(m,1,2,33) #5
    colorear(m,1,3,32)
    colorear(m,1,4,32)
    print()
    print("____________________________________")
    print("|  ", end = "")
    colorear(m,2,0,36) #4
    colorear(m,2,1,33)
    colorear(m,2,2,33) 
    colorear(m,2,3,33)
    colorear(m,2,4,32)
    print()
    print("____________________________________")
    print("|  ", end = "")
    colorear(m,3,0,36) #4
    colorear(m,3,1,36)
    colorear(m,3,2,33) 
    colorear(m,3,3,31) #3
    colorear(m,3,4,31)
    print()
    print("____________________________________")
    print("|  ", end = "")
    colorear(m,4,0,36) 
    colorear(m,4,1,36)
    colorear(m,4,2,31) 
    colorear(m,4,3,31) 
    colorear(m,4,4,31)
    print()
    print("____________________________________")

inicio()
instru()
nivel,n_res = nivel()
sudoku = sudoku()
esco = esconder(nivel,sudoku)
resp = 0
parar = ""
while parar != "LISTO" and parar != "RESPUESTA":
    imprimir(esco)
    responder(esco)
    resp +=1
    if resp >= n_res:
        print("Parece que ya estas cerca de terminar escribe LISTO para revisar, RESPUESTA para rendirte, o cualquier cosa para continuar")
        parar = input(">>>")
limpia()
verificar(sudoku,esco)
